﻿using System.Collections.Generic;
using System.Threading.Tasks;
using XinMai.Mapper.Req;
using XinMai.Models;

namespace XinMai.IBLL
{
    public interface IPersonBLL<T> where T:class
    {
        /// <summary>
        /// 通过Code获取OpenId
        /// </summary> 
        /// <returns></returns>
        Task<EntityResponse<Dictionary<string, object>>> OpenId(ReqOpenIdDto queryDto,ReqBaseDto baseDto);

        /// <summary>
        /// 用户注册
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        Task<EntityResponse<T>> Register(ReqRegisterDto queryDto,ReqBaseDto baseDto);

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        Task<EntityResponse<T>> UpdateInfo(object obj, ReqBaseDto baseDto);

        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        Task<EntityResponse<T>> Login(ReqOpenCodeDto queryDto, ReqBaseDto baseDto);

    }
}
