﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using XinMai.Mapper.Req;
using XinMai.Mapper.Res;
using XinMai.Models;

namespace XinMai.IBLL
{
    public interface ICityOrPointAreaBLL
    {
        /// <summary>
        /// 通过半径找出用户或地标等信息
        /// </summary>
        /// <param name="queryDto"></param>
        /// <param name="baseDto"></param>
        /// <returns></returns>
        Task<ListResponse<ResMapMarkDto>> UsersList(ReqPointAreaDto queryDto,ReqBaseDto baseDto);


    }
}
