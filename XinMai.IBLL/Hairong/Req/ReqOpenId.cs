﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.IBLL.Hairong.Req
{
    public class ReqOpenId : BaseReqDto
    {
        public string AppId { get; set; }
        public string Secret { get; set; }
        public string Code { get; set; } 
    }
}
