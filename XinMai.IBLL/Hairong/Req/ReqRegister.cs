﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.IBLL.Hairong.Req
{
    public class ReqRegister:BaseReqDto
    {
        public string Text { get; set; }
        public string Key { get; set; }
        public string IV { get; set; } 
        public ReqUserInfo UserInfo { get; set; }
    }

    /// <summary>
    /// 用户注册
    /// </summary>
    public class ReqUserInfo
    { 
        public string NickName { get; set; }
        public string OpenId { get; set; }
        public int Sex { get; set; }
        public int PushId { get; set; }
        public string Mobile { get; set; }
        public string ImagePath { get; set; }
        public string CityCode { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
