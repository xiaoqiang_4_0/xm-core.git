﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.IBLL
{
    /// <summary>
    /// 共同请求参数
    /// </summary>
    public class BaseReqDto
    {
        /// <summary>
        /// 执行方法名
        /// </summary>
        public string OpenApi { get; set; }
    }

    /// <summary>
    /// 分页请求参数
    /// </summary>
    public class PageBaseReqDto:BaseReqDto
    {
        /// <summary>
        /// 每页条数
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// 关键词
        /// </summary>
        public string Key { get; set; }
    }

    /// <summary>
    /// 主键请求参数
    /// </summary>
    public class SingleReqDto : BaseReqDto
    {
        public long Id { get; set; }
    }
}
