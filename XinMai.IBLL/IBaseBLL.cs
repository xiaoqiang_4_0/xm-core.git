﻿using System.Threading.Tasks;
using XinMai.Mapper.Req;
using XinMai.Models;

namespace XinMai.IBLL
{
    /// <summary>
    /// 基类接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseBLL<T> where T : class
    {

        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        Task<ListResponse<T>> GetList(ReqBaseDto req);

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ListResponse<T>> GetPageList(ReqPageBaseDto queryDto,ReqBaseDto baseDto);

        /// <summary>
        /// 删除单个实体
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<EntityResponse<bool>> DeleteSingle(ReqSingleDto queryDto, ReqBaseDto baseDto);

        /// <summary>
        /// 获取单个实体
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<EntityResponse<T>> GetSingle(ReqSingleDto queryDto, ReqBaseDto baseDto);

       
    }
}
