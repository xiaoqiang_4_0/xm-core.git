﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.BLL.Models
{
    /// <summary>
    /// 手机号码获取
    /// </summary>
    public class PhoneNumber
    {
        /// <summary>
        /// 手机号码
        /// </summary>
        public string phoneNumber { get; set; }
        /// <summary>
        /// 纯粹的电话号码
        /// </summary>
        public string purePhoneNumber { get; set; }
        /// <summary>
        /// 国家代码
        /// </summary>
        public string countryCode { get; set; }
        /// <summary>
        /// APPID和时间戳
        /// </summary>
        public Watermark watermark { get; set; }
    }

    public class Watermark
    {
        /// <summary>
        /// 时间戳
        /// </summary>
        public int timestamp { get; set; }
        /// <summary>
        /// APPID
        /// </summary>
        public string appid { get; set; }
    }

}
