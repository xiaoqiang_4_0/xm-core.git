﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using XinMai.Tools;

namespace XinMai.BLL.Models
{

    public class TxDistances
    {
        private const string KEY = "X52BZ-D7XCD-EZE4Y-PSJCU-QDN3V-23FI7";

        /// <summary>
        /// 获取两个点距离
        /// </summary>
        /// <param name="from">出发点</param>
        /// <param name="to">目的地，多个目的地用分号隔开</param>
        /// <param name="mode">driving（驾车）、walking（步行）</param>
        /// <returns></returns>
        public static Root Distances(string from, string to, string mode = "driving")
        {
            string url = "https://apis.map.qq.com/ws/distance/v1/?mode={0}&from={1}8&to={2}&key={3}";
            string result = HttpHelper.Get(string.Format(url, mode, from, to, KEY));
            return JsonSerialize.JsonDeserialize<Root>(result);
        }

        public static WebRoot City(string point)
        {
            string url = "https://apis.map.qq.com/ws/geocoder/v1/?location={0}&key={1}&get_poi=1";
            string result = HttpHelper.Get(string.Format(url, point, KEY));
            var obj = JsonSerialize.JsonDeserialize<WebRoot>(result);
            return obj;
        }

        public static bool Compare(string from, string to, int radius = 5)
        {
            bool flag = false;
            Root root = Distances(from, to);
            if (root.status == 0)
            {
                flag = root.result.elements[0].distance / 1000 < radius;
            }
            return flag;
        }
    }

    #region 参数配置
    /// <summary>
    /// 定位
    /// </summary>
    public class XPoint
    {
        /// <summary>
        /// 纬度
        /// </summary>
        public double lat { get; set; }
        /// <summary>
        /// 经度
        /// </summary>
        public double lng { get; set; }
    }

    /// <summary>
    /// 经纬度坐标<see cref="Point"/>类 
    /// </summary>    
    [Description("经纬度坐标")]
    public class Point
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="x">经度</param>
        /// <param name="y">纬度</param>
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }
        /// <summary>
        /// 经度
        /// </summary>
        private double x;
        /// <summary>
        /// 经度
        /// </summary>
        public double X
        {
            get { return x; }
            set { x = value; }
        }
        /// <summary>
        /// 纬度
        /// </summary>
        private double y;
        /// <summary>
        /// 纬度
        /// </summary>
        public double Y
        {
            get { return y; }
            set { y = value; }
        }

    }

    public class Element
    {
        /// <summary>
        /// 起点
        /// </summary>
        public XPoint from { get; set; }
        /// <summary>
        /// 终点
        /// </summary>
        public XPoint to { get; set; }
        /// <summary>
        /// 距离
        /// </summary>
        public int distance { get; set; }
        /// <summary>
        /// 持续时间
        /// </summary>
        public int duration { get; set; }
    }

    public class Result
    {
        /// <summary>
        /// 元素
        /// </summary>
        public List<Element> elements { get; set; }
    }

    public class Root
    {
        /// <summary>
        /// 状态
        /// </summary>
        public int status { get; set; }
        /// <summary>
        /// 消息结果
        /// </summary>
        public string message { get; set; }
        /// <summary>
        /// 返回结果集
        /// </summary>
        public Result result { get; set; }
    }
    #endregion

    public class WebRoot
    {
        /// <summary>
        /// 状态
        /// </summary>
        public int status { get; set; }
        /// <summary>
        /// 消息结果
        /// </summary>
        public string message { get; set; }
        /// <summary>
        /// 消息结果编码
        /// </summary>
        public string request_id { get; set; }
        /// <summary>
        /// 返回结果集
        /// </summary>
        public WebResult result { get; set; }
    }

    public class WebResult
    {
        /// <summary>
        /// 定位
        /// </summary>
        public XPoint location { get; set; }
        /// <summary>
        /// 详细地址
        /// </summary>
        public string address { get; set; }
        /// <summary>
        /// 具体信息
        /// </summary>
        public Address_Component address_component { get; set; }
        /// <summary>
        /// 地点信息
        /// </summary>
        public Ad_Info ad_info { get; set; }
    }

    public class Address_Component
    {
        /// <summary>
        ///  
        /// </summary>
        public string nation { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public string province { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public string city { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public string district { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public string street { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public string street_number { get; set; }
    }

    public class Ad_Info
    {
        /// <summary>
        /// 
        /// </summary>
        public string nation_code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string adcode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string city_code { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Point location { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public string nation { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public string province { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public string city { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public string district { get; set; }
    }
}
