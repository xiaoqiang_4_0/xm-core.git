﻿  //------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。 
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//     如存在本生成代码外的新需求，请在相同命名空间下创建同名分部类实现
// </auto-generated>
//
// <copyright>
//        Copyright(c)2021 XinMai.All rights reserved.
//        CLR版本：4.5
//        开发组织：上海辛麦信息科技@信息中心
//        公司名称：上海辛麦信息科技 
//        所属项目：T4模板引擎
//        当前工程：XinMai.BLL.{0} -- 具体实现数据交互API
//        生成时间：2021-04-29 16:35:56:287
//        作者：Dragon（周龙） QQ：1033085514，手机：15300700700，微信：15300700700 
//        地址：上海市-奉贤区
// </copyright>
//------------------------------------------------------------------------------   


using AutoMapper; 
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;   
using System.Threading.Tasks; 
using XinMai.Extensions;
using XinMai.IBLL;
using XinMai.IBLL.Saasbase; 
using XinMai.Models;  
using XinMai.Entity.Saasbase;
using XinMai.IDAL.Saasbase;
using XinMai.Mapper.Req;
  
namespace XinMai.BLL.Saasbase 
{
    /*
	API接口实现类 --
	*/  
    /// <summary>
    ///  API接口
    /// </summary> 
    [Description(@"")] 
    public partial class ModuleCategoryBLL : IBaseBLL<ModuleCategory>, IModuleCategoryBLL
    { 

        private readonly  IModuleCategoryAppService _moduleCategory;
        private readonly IMapper _mapper;
        public ModuleCategoryBLL(IModuleCategoryAppService moduleCategory, IMapper mapper)
        {
            _moduleCategory = moduleCategory;
            _mapper = mapper;
        }
            
        #region 共性方法
        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<ListResponse<ModuleCategory>> GetList(ReqBaseDto req)
        {
            ListResponse<ModuleCategory> entity = new ListResponse<ModuleCategory>();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var result = await _moduleCategory.GetListAsync();
            sw.Stop();
            entity.Code = result != null ? 0 : -1;
            entity.Data = result;
            entity.Message = result != null ? "获取数据成功！" : "获取数据失败！";
            entity.OpenApi = req.OpenApi;
            entity.TimeSpan = sw.ElapsedMilliseconds;
            return entity;
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<ListResponse<ModuleCategory>> GetPageList(ReqPageBaseDto queryDto,ReqBaseDto baseDto)
        {
            ListResponse<ModuleCategory> entity = new ListResponse<ModuleCategory>();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            #region 模糊搜索条件设置
            var where = PredicateBuilder.New<ModuleCategory>(true);
            where.And(p => !p.IsFrozen.Value);//过滤已删除数据
            System.Linq.Expressions.Expression<Func<ModuleCategory, DateTime?>> sort = s => s.CreateTime;
            if (queryDto.Key.IsNotEmpty())
            {
                    where = where.Or(p => p.Name.Contains(queryDto.Key));  
                where = where.Or(p => p.ImagePath.Contains(queryDto.Key));  
                where = where.Or(p => p.Mark.Contains(queryDto.Key));  
            }
            #endregion
            var result = await _moduleCategory.GetPageListAsync(queryDto.Page, queryDto.PageSize, where, sort, FreeSql.SortingType.Descending);
            sw.Stop();
            entity.Code = result.count >0? 0 : -1;
            entity.Data = result.items;
            entity.Message = result.count>0 ? "获取数据成功！" : "获取数据失败！";
            entity.OpenApi = baseDto.OpenApi;
            entity.TotalCount = result.count;
            entity.TimeSpan = sw.ElapsedMilliseconds;
            return entity;
        }

        /// <summary>
        /// 获取单个实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<EntityResponse<ModuleCategory>> GetSingle(ReqSingleDto queryDto,ReqBaseDto baseDto)
        {
            EntityResponse<ModuleCategory> entity = new EntityResponse<ModuleCategory>();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var result = await _moduleCategory.GetSingleAsync(queryDto.Id);
            sw.Stop();
            entity.Code = result != null ? 0 : -1;
            entity.Data = result;
            entity.Message = result != null ? "获取数据成功！" : "获取数据失败！";
            entity.OpenApi = baseDto.OpenApi;
            entity.TimeSpan = sw.ElapsedMilliseconds;
            return entity;
        }

        /// <summary>
        /// 删除单个实体
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<EntityResponse<bool>> DeleteSingle(ReqSingleDto queryDto,ReqBaseDto baseDto)
        {
            EntityResponse<bool> entity = new EntityResponse<bool>();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var result = await _moduleCategory.GetSingleAsync(queryDto.Id);
            result.IsFrozen = true;
            var flag = await _moduleCategory.UpdateAsync(result);
            sw.Stop();
            entity.Code = flag ? 0 : -1;
            entity.Data = flag;
            entity.Message = flag ? "删除数据成功！" : "删除数据失败！";
            entity.OpenApi = baseDto.OpenApi;
            entity.TimeSpan = sw.ElapsedMilliseconds;
            return entity;
        }
        #endregion

        #region 自定义方法
        #endregion
    }
}

