﻿using System;
using System.ComponentModel;

namespace XinMai.Enum.Hairong
{
    /// <summary>
    /// 支付结算类型
    /// </summary>
    public enum PayTypeEnum
    {
        /// <summary>
        /// 一次性
        /// </summary>
        [Description("一次性")]
        OnlyOne = 0,
        /// <summary>
        /// 小时
        /// </summary>
        [Description("小时")]
        Hour = 1,
        /// <summary>
        /// 日
        /// </summary>
        [Description("日")]
        Day = 2,
        /// <summary>
        /// 周
        /// </summary>
        [Description("周")]
        Week = 3,
        /// <summary>
        /// 半个月
        /// </summary>
        [Description("半个月")]
        HalfMonth = 4,
        /// <summary>
        /// 月
        /// </summary>
        [Description("月")]
        Month = 5,
        /// <summary>
        /// 季
        /// </summary>
        [Description("季")]
        Season = 6,
        /// <summary>
        /// 半年
        /// </summary>
        [Description("半年")]
        HalfYear = 7,
        /// <summary>
        /// 年
        /// </summary>
        [Description("年")]
        Year = 8
    }
}
