﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.Mapper.Hairong.Req
{
    public class ReqJobsQueryDto
    {
        public string OpenId { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string JobName { get; set; }
        public string JobDes { get; set; }
        public int? PayType { get; set; }
        public decimal? Money { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Contact { get; set; }
        public string Mobile { get; set; }
        public string GatherTime { get; set; }
        public string CityCode { get; set; }
        public string GatherAddr { get; set; }
        public string Mark { get; set; }
    }
}
