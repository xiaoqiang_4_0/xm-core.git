﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.Mapper.Hairong.Req
{
    public class ReqInviteQueryDto
    {
        public string OpenId { get; set; }
        public int Id { get; set; }
    }
}
