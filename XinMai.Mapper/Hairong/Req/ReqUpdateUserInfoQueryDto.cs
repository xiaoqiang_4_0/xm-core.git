﻿using XinMai.Mapper.Req;

namespace XinMai.Mapper.Hairong.Req
{
    /// <summary>
    /// 用户修改
    /// </summary>
    public class ReqUpdateUserInfoQueryDto : ReqUpdateUserInfo
    {
        public string Job { get; set; }
    }
}
