﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.Mapper.Hairong.Req
{
   public class ReqApplyJobsQueryDto
    {
        public string OpenId { get; set; }
        public int JobsId { get; set; }
        public string RealName { get; set; }
        public string Mobile { get; set; }
        public string Speciality { get; set; }
    }
}
