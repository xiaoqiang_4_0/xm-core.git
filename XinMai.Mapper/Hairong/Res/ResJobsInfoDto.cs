﻿using System;
using System.Collections.Generic;
using System.Text;
using XinMai.Mapper.Res;

namespace XinMai.Mapper.Hairong.Res
{
   public class ResJobsInfoDto
    {
        public long Id { get; set; }
        public string OpenId { get; set; }
        public long CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string JobName { get; set; }
        public string JobDes { get; set; }
        public string PayTypeName { get; set; }
        public decimal Money { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int Click { get; set; }
        public int Favorite { get; set; }
        public string GatherTime { get; set; }
        public string CityCode { get; set; }
        public string GatherAddr { get; set; }
        public string Mark { get; set; }
        public string Distance { get; set; }
        public ResMapMarkDto Map { get; set; }
        public ResUserInfoDto UserInfo { get; set; }
    }
}
