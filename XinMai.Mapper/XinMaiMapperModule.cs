﻿
using Microsoft.Extensions.DependencyInjection;
using System;
using XinMai.Mapper.Profile;

namespace XinMai.Mapper
{
    public static class XinMaiMapperModule
    {
        //实体映射
        public static IServiceCollection AddAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(MapperProfile));
            return services;
        }
    }
}
