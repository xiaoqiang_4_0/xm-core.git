﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.Mapper.Res
{
   public class ResUserInfoDto
    {
        public long Id { get; set; } 
        public string  Code { get; set; } 
        public string ImagePath { get; set; }
        public string RealName { get; set; }
        public string NickName { get; set; }
        public int? Sex { get; set; }
        public DateTime? Birthday { get; set; }
        public string Mobile { get; set; }
        public string CityCode { get; set; }
        public int? PushId { get; set; }
        public string Job { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
        public int Click { get; set; }
        public string Mark { get; set; }
        public string Address { get; set; }
        public string Distance { get; set; }
    }
}
