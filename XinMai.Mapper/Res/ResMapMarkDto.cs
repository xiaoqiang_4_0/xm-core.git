﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.Mapper.Res
{
    public class ResMapMarkDto
    {
        /// <summary>
        /// 
        /// </summary>
        public long id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double latitude { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double longitude { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string iconPath { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public CallOut callout { get; set; }
    }

    public class CallOut
    {
        public string content { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string bgColor { get; set; } = "#fff";
        /// <summary>
        /// 
        /// </summary>
        public string padding { get; set; } = "5px";
        /// <summary>
        /// 
        /// </summary>
        public string borderRadius { get; set; } = "2px";
        /// <summary>
        /// 
        /// </summary>
        public string borderWidth { get; set; } = "1px";
        /// <summary>
        /// 
        /// </summary>
        public string borderColor { get; set; } = "#07c160";
    }
}
