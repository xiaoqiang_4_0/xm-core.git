﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.Mapper.Req
{
    public class ReqOpenIdSearchDto : ReqPageBaseDto
    {
        public double? Lat { get; set; }

        public double? Lng { get; set; }
    }
}
