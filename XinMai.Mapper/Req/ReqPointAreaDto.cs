﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.Mapper.Req
{
    /// <summary>
    /// 半径范围
    /// </summary>
    public class ReqPointAreaDto
    {
        public double Lat { get; set; }

        public double Lng { get; set; }

        public string CityCode { get; set; }

        public int Radius { get; set; } = 5;
    }
}
