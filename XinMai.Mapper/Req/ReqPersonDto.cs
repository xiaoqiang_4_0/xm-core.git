﻿using System;
using XinMai.Mapper;

namespace XinMai.Mapper.Req
{
    /// <summary>
    /// 微信获取OpenId
    /// </summary>
    public class ReqOpenIdDto {  
        public string AppId { get; set; }
        public string Secret { get; set; }
        public string Code { get; set; }
    }

    /// <summary>
    /// 通过OenId获取用户信息
    /// </summary>
    public class ReqOpenCodeDto  
    {
        /// <summary>
        /// 快捷编码
        /// </summary>
        public string OpenId { get; set; }
    }

    /// <summary>
    /// 用户注册
    /// </summary>
    public class ReqRegister : ReqUserInfo
    {
        public string Text { get; set; }
        public string Key { get; set; }
        public string IV { get; set; }

    }

    /// <summary>
    /// 用户注册
    /// </summary>
    public class ReqRegisterDto 
    {
        public string Text { get; set; }
        public string Key { get; set; }
        public string IV { get; set; }
        public ReqUserInfo UserInfo { get; set; }
    }

    /// <summary>
    /// 用户注册
    /// </summary>
    public class ReqUserInfo
    {
        public string NickName { get; set; }
        public string OpenId { get; set; }
        public int Sex { get; set; }
        public int PushId { get; set; }
        public string Mobile { get; set; }
        public string ImagePath { get; set; }
        public string CityCode { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
    }

    public class ReqUpdateUserInfo 
    {
        public string RealName { get; set; }
        public string IDCard { get; set; }
        public string Birthday { get; set; } 
        public string Mobile { get; set; }
        public string NickName { get; set; }
        public string CityCode { get; set; }
        public string OpenId { get; set; }
        public string ImagePath { get; set; }
    }

   
}
