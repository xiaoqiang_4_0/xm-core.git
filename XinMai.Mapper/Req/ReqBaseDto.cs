﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.Mapper.Req
{
    /// <summary>
    /// 共同请求参数
    /// </summary>
    public class ReqBaseDto
    {
        /// <summary>
        /// 执行方法名
        /// </summary>
        public string OpenApi { get; set; }
    }

  

    /// <summary>
    /// 主键请求参数
    /// </summary>
    public class ReqSingleDto
    {
        public long Id { get; set; }
    }

    public class ReqSingleExtDto:ReqSingleDto
    {
        public double Lat { get; set; }

        public double Lng { get; set; }
    }

    public class ReqSingleOpenIdDto : ReqSingleDto
    {
        public string OpenId { get; set; }
    }
}
