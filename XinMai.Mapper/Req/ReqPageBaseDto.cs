﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.Mapper.Req
{
    /// <summary>
    /// 分页请求参数
    /// </summary>
    public class ReqPageBaseDto
    {
        /// <summary>
        /// 每页条数
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// 关键词，单个搜索时可替代指定数据字段
        /// </summary>
        public string Key { get; set; }
    }
}
