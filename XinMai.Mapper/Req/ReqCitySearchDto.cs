﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.Mapper.Req
{
    public class ReqCitySearchDto : ReqPageBaseDto
    {
        public double? Lat { get; set; }

        public double? Lng { get; set; }

        public string CityCode { get; set; } 
    }
}
