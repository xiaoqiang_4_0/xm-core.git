﻿using XinMai.Mapper.Hairong.Req;
using XinMai.Mapper.Req;

namespace XinMai.Mapper.Profile
{
    public class MapperProfile : AutoMapper.Profile
    {
        public MapperProfile()
        {
            //注册时转换
            CreateMap<ReqUserInfo, Entity.Hairong.UserInfo>();
            //前台接收转换
            CreateMap<ReqRegisterDto, ReqUserInfo>();
            ///前台修改用户信息
            CreateMap<ReqUpdateUserInfo, ReqUpdateUserInfoQueryDto>();
        }
    }
}
