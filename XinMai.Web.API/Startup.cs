using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using XinMai.FreeSql;
using XinMai.Mapper;
using XinMai.Web.API.Extensions;
using XinMai.Web.API.Modules;
using XinMai.Web.Core.Ext;
using XinMai.Web.Core.Ext.Extensions;
using XinMai.Web.Core.Net;

namespace XinMai.Web.API
{
    public class Startup
    {
        public static IFreeSql Fsql { get; private set; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Fsql = new MultiFreeSql();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddJsonOptions(c =>
            {
                c.JsonSerializerOptions.Converters.Add(new UnixTimestampConvertor()); 
            }); 

            services.AddAutoMapper();

            services.AddCustomeSwagger();

            services.AddMultFreeSql(Configuration, Fsql, "MultFreeSql");

            services.AddAspNetCore();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseLoggerMiddleware();

            //异常中间件
            app.UseExceptionMiddleware();
            
            //IOC注入中间件
            app.UseAutofacIocManager();

            app.CustomeSwagger();

            app.UseRouting();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new Modules.ApplicationModule());
        }
    }
}
