﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XinMai.Web.API 
{
    public class AuthorizeAttribute :  Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var principle = context.HttpContext.User;
            if (!principle.Identity.IsAuthenticated)
            {
                // not authenticated through jwt validation
                //context.Result = new JsonResult(new { message = "非法请求" }) { StatusCode = StatusCodes.Status401Unauthorized };
            }
        }
    }
}
