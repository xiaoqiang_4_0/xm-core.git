﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace XinMai.Web.API.Extensions
{
    /// <summary>
    /// Swagger组件
    /// </summary>
    public static class SwaggerExtensions
    {
        /// <summary>
        /// Swagger组件注册
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddCustomeSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("default", new OpenApiInfo { Title = "默认", Version = "Xinmai", Description = "辛麦信息平台 API" });
                options.SwaggerDoc("Hairong", new OpenApiInfo { Title = "普工招聘系统", Version = "Hairong", Description = "辛麦信息平台普工招聘 API" });
                options.SwaggerDoc("Saasbase", new OpenApiInfo { Title = "多租户系统", Version = "Saasbase", Description = "辛麦信息平台多租户 API" });
                options.SwaggerDoc("Shop", new OpenApiInfo { Title = "商城系统", Version = "Shop", Description = "辛麦信息平台商城系统 API" });
                options.SwaggerDoc("Order", new OpenApiInfo { Title = "订单系统", Version = "Order", Description = "辛麦信息平台订单系统 API" });
                options.SwaggerDoc("Paycenter", new OpenApiInfo { Title = "支付中心系统", Version = "Paycenter", Description = "辛麦信息平台支付中心 API" });
                #region Xml File Import
                var xmlFiles = new List<string>
                {
                    "XinMai.Web.API.xml",
                };
                foreach (var xmlFile in xmlFiles)
                {
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                    options.IncludeXmlComments(xmlPath, true);
                }
                #endregion 
            });

            return services;
        }

        /// <summary>
        /// Swagger组件定义
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder CustomeSwagger(this IApplicationBuilder app)
        {
            app.UseSwagger(c =>
            {
                c.RouteTemplate = "swagger/{documentName}/swagger.json";
            }).UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/default/swagger.json", "默认");
                c.SwaggerEndpoint("/swagger/Hairong/swagger.json", "普工招聘模块");
                c.SwaggerEndpoint("/swagger/Saasbase/swagger.json", "多租户模块");
                c.SwaggerEndpoint("/swagger/Shop/swagger.json", "多商户商品模块");
                c.SwaggerEndpoint("/swagger/Order/swagger.json", "多商户订单模块");
                c.SwaggerEndpoint("/swagger/Paycenter/swagger.json", "支付中心模块");
            }); ;

            return app;
        }
    }
}
