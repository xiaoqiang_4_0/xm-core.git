﻿  //------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。 
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//     如存在本生成代码外的新需求，请在相同命名空间下创建同名分部类实现
// </auto-generated>
//
// <copyright>
//        Copyright(c)2021 XinMai.All rights reserved.
//        CLR版本：4.5
//        开发组织：上海辛麦信息科技@信息中心
//        公司名称：上海辛麦信息科技 
//        所属项目：T4模板引擎
//        当前工程：XinMai.Web.API.Controllers -- 具体实现数据交互API
//        生成时间：2021-04-29 16:35:52:840
//        作者：Dragon（周龙） QQ：1033085514，手机：15300700700，微信：15300700700 
//        地址：上海市-奉贤区
// </copyright>
//------------------------------------------------------------------------------   
 
using AutoMapper;
using System;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;
using System.Threading.Tasks;
/*==============自定义=================*/
using XinMai.Entity.Saasbase; 
using XinMai.IBLL.Saasbase;
using XinMai.Models;
using XinMai.Web.Core.Net.Mvc;
using XinMai.Mapper.Req; 

namespace XinMai.Web.API.Controllers 
{
    /*
	API接口调用类 --租户拥有模块使用权表
	*/  
    /// <summary>
    /// 租户拥有模块使用权表 API接口
    /// </summary>
    [Route("api/saasbase/[controller]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "Saasbase")]
    [Description(@"实现具体接口 -- 租户拥有模块使用权表")]
	public partial class  TenantModuleController : XinMaiController
    { 
         private readonly IMapper _mapper;
         private readonly ITenantModuleBLL _TenantModuleBLL;

         /// <summary>
         /// 构造函数
         /// </summary>
         /// <param name="sp">Service管道</param> 
         /// <param name="mapper">映射工具</param>
         /// <param name="TenantModuleBLL">具体服务</param>
         public TenantModuleController(IServiceProvider sp, IMapper mapper, ITenantModuleBLL TenantModuleBLL) : base(sp)
         {
             _mapper = mapper;
            _TenantModuleBLL = TenantModuleBLL;  
         }

        #region 通用的API接口
        /// <summary>
        /// 获取全部数据
        /// </summary> 
        /// <returns></returns>
        [HttpGet("list")]
        [Authorize]
        public async Task<ListResponse<TenantModule>> GetList()
        {
            return await _TenantModuleBLL.GetList(new ReqBaseDto { OpenApi = "TenantModule/GetList" });
        }

         /// <summary>
        /// 分页
        /// </summary> 
        /// <returns></returns>
        [HttpGet("page")]
        [Authorize]
        public async Task<ListResponse<TenantModule>> GetPageList([FromQuery] int page, int pageSize, string key)
        {
            ReqPageBaseDto req = new ReqPageBaseDto()
            {
                Key = key, 
                Page = page,
                PageSize = pageSize
            };
            return await _TenantModuleBLL.GetPageList(req,new ReqBaseDto{ OpenApi = "TenantModule/GetPageList"});
        }

        /// <summary>
        /// 获取单个实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("single")]
        [Authorize]
        public async Task<EntityResponse<TenantModule>> GetSingle([FromQuery] long id)
        {
            return await _TenantModuleBLL.GetSingle(new ReqSingleDto { Id = id },new ReqBaseDto{ OpenApi = "TenantModule/GetSingle" });
        }

        /// <summary>
        /// 删除单个实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete")]
        [Authorize]
        public async Task<EntityResponse<bool>> DeleteSingle([FromQuery] long id)
        {
            return await _TenantModuleBLL.DeleteSingle(new ReqSingleDto { Id = id },new ReqBaseDto{ OpenApi = "TenantModule/DeleteSingle" });
        }
        #endregion

        #region 自定义使用接口

        #endregion
    }
}

