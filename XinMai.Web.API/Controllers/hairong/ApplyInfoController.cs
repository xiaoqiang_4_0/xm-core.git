﻿  //------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。 
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//     如存在本生成代码外的新需求，请在相同命名空间下创建同名分部类实现
// </auto-generated>
//
// <copyright>
//        Copyright(c)2021 XinMai.All rights reserved.
//        CLR版本：4.5
//        开发组织：上海辛麦信息科技@信息中心
//        公司名称：上海辛麦信息科技 
//        所属项目：T4模板引擎
//        当前工程：XinMai.Web.API.Controllers -- 具体实现数据交互API
//        生成时间：2021-04-26 00:06:49:989
//        作者：Dragon（周龙） QQ：1033085514，手机：15300700700，微信：15300700700 
//        地址：上海市-奉贤区
// </copyright>
//------------------------------------------------------------------------------   
 
using AutoMapper;
using System;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;
using System.Threading.Tasks;
/*==============自定义=================*/
using XinMai.Entity.Hairong; 
using XinMai.IBLL.Hairong;
using XinMai.Models;
using XinMai.Web.Core.Net.Mvc;
using XinMai.Mapper.Req; 

namespace XinMai.Web.API.Controllers 
{
    /*
	API接口调用类 --报名表
	*/  
    /// <summary>
    /// 报名表 API接口
    /// </summary>
    [Route("api/hairong/[controller]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "Hairong")]
    [Description(@"实现具体接口 -- 报名表")]
	public partial class  ApplyInfoController : XinMaiController
    { 
         private readonly IMapper _mapper;
         private readonly IApplyInfoBLL _ApplyInfoBLL;

         /// <summary>
         /// 构造函数
         /// </summary>
         /// <param name="sp">Service管道</param> 
         /// <param name="mapper">映射工具</param>
         /// <param name="ApplyInfoBLL">具体服务</param>
         public ApplyInfoController(IServiceProvider sp, IMapper mapper, IApplyInfoBLL ApplyInfoBLL) : base(sp)
         {
             _mapper = mapper;
            _ApplyInfoBLL = ApplyInfoBLL;  
         }

        #region 通用的API接口
        /// <summary>
        /// 获取全部数据
        /// </summary> 
        /// <returns></returns>
        [HttpGet("list")]
        [Authorize]
        public async Task<ListResponse<ApplyInfo>> GetList()
        {
            return await _ApplyInfoBLL.GetList(new ReqBaseDto { OpenApi = "ApplyInfo/GetList" });
        }

         /// <summary>
        /// 分页
        /// </summary> 
        /// <returns></returns>
        [HttpGet("page")]
        [Authorize]
        public async Task<ListResponse<ApplyInfo>> GetPageList([FromQuery] int page, int pageSize, string key)
        {
            ReqPageBaseDto req = new ReqPageBaseDto()
            {
                Key = key, 
                Page = page,
                PageSize = pageSize
            };
            return await _ApplyInfoBLL.GetPageList(req,new ReqBaseDto{ OpenApi = "ApplyInfo/GetPageList"});
        }

        /// <summary>
        /// 获取单个实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("single")]
        [Authorize]
        public async Task<EntityResponse<ApplyInfo>> GetSingle([FromQuery] long id)
        {
            return await _ApplyInfoBLL.GetSingle(new ReqSingleDto { Id = id },new ReqBaseDto{ OpenApi = "ApplyInfo/GetSingle" });
        }

        /// <summary>
        /// 删除单个实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete")]
        [Authorize]
        public async Task<EntityResponse<bool>> DeleteSingle([FromQuery] long id)
        {
            return await _ApplyInfoBLL.DeleteSingle(new ReqSingleDto { Id = id },new ReqBaseDto{ OpenApi = "ApplyInfo/DeleteSingle" });
        }
        #endregion

        #region 自定义使用接口

        #endregion
    }
}

