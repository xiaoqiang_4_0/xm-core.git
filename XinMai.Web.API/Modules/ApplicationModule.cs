﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XinMai.Core.Helper;
using XinMai.FreeSql;

namespace XinMai.Web.API.Modules
{
    public class ApplicationModule :  Core.Ext.ApplicationModule
    {
        /// <summary>
        /// 加载
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);//Autofac 基类创建
            var assemblies = RuntimeHelper.GetAllAssemblies();
            builder.RegisterGeneric(typeof(SingleBasicRepository<>))
                .As(typeof(IRepository<>))
                .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(MultiBasicRepository<>))
               .As(typeof(IMultiRepository<>))
               .InstancePerLifetimeScope();

            //批量注册业务层接口
            builder.RegisterAssemblyTypes(assemblies.ToArray())
              .Where(type => typeof(IBLL.IBussinessAppService).IsAssignableFrom(type) && !type.IsAbstract)
              .AsImplementedInterfaces()
              .SingleInstance();
        }
    }
}
