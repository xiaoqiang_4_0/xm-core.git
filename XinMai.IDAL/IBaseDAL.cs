﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using XinMai.FreeSql;

namespace XinMai.IDAL
{
    /// <summary>
    /// 业务基础接口
    /// </summary>
    /// <typeparam name="T">具体实现类（表）</typeparam>
    public interface IBaseDAL<T> where T : class
    {
        #region 同步调用方法
        T GetSingle(long id);

        T GetSingle(Expression<Func<T, bool>> predicate);

        List<T> GetList();

        long Count(Expression<Func<T, bool>> predicate);

        List<T> GetList(Expression<Func<T, bool>> predicate);

        (List<T> items, long count) GetPageList<TMember>(int page,
            int pageSize,
            Expression<Func<T, bool>> predicate = null,
            Expression<Func<T, TMember>> sorting = null,
            SortingType sortingType = SortingType.Ascending);

        T Insert(T t);

        List<T> Insert(List<T> t);

        bool Update(T t);

        bool Update(Expression<Func<T, bool>> predicate, Action<T> action);

        bool Update(List<T> t);
 
        bool Transaction(Action handler);
        #endregion

        #region 异步操作方法
        Task<T> GetSingleAsync(long id);

        Task<T> GetSingleAsync(Expression<Func<T, bool>> predicate);

        Task<long> CountAsync(Expression<Func<T, bool>> predicate);

        Task<List<T>> GetListAsync();

        Task<List<T>> GetListAsync(Expression<Func<T, bool>> predicate);

        Task<(List<T> items, long count)> GetPageListAsync(int page,
            int pageSize, Expression<Func<T, bool>> predicate = null);

        Task<(List<T> items, long count)> GetPageListAsync<TMember>(int page,
            int pageSize,
            Expression<Func<T, bool>> predicate = null,
            Expression<Func<T, TMember>> sorting = null,
            SortingType sortingType = SortingType.Ascending);

        Task<T> InsertAsync(T t);

        Task<List<T>> InsertAsync(List<T> t);

        Task<bool> UpdateAsync(T t);

        Task<bool> UpdateAsync(Expression<Func<T, bool>> predicate, Action<T> action);

        Task<bool> UpdateAsync(List<T> t);
        #endregion
    }
}
