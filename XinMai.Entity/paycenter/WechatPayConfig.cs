﻿  //------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。 
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//     如存在本生成代码外的新需求，请在相同命名空间下创建同名分部类实现
// </auto-generated>
//
// <copyright>
//        Copyright(c)2021 XinMai.All rights reserved.
//        CLR版本：4.5
//        开发组织：上海辛麦信息科技@信息中心
//        公司名称：上海辛麦信息科技 
//        所属项目：T4模板引擎
//        当前工程：XinMai.Entity.Paycenter -- 数据实体
//        生成时间：2021-04-29 16:50:53:512
//        作者：Dragon（周龙） QQ：1033085514，手机：15300700700，微信：15300700700 
//        地址：上海市-奉贤区
// </copyright>
//------------------------------------------------------------------------------   

using System;
using System.ComponentModel; 
using FreeSql.DataAnnotations;
namespace XinMai.Entity.Paycenter 
{
    /*
	数据库表映射实体 --
	*/ 
	[Description(@"分布式代码 -- ")]
	public partial class WechatPayConfig : Entity<long>
    {
        /// <summary>
        /// 多租户惟一编号 
        /// </summary>
        [Description(@"多租户惟一编号")]
        public int? TenantId { get; set; }
        /// <summary>
        /// 商户惟一编码 字符长度不能超过64个字符
        /// </summary>
        [Description(@"商户惟一编码")]
        [Column(StringLength = 64)]
        public string MchCode { get; set; }
        /// <summary>
        /// 应用ID号 字符长度不能超过32个字符
        /// </summary>
        [Description(@"应用ID号")]
        [Column(StringLength =32)]
        public string AppId { get; set; }
        /// <summary>
        /// 应用秘钥 字符长度不能超过255个字符
        /// </summary>
        [Description(@"应用秘钥")]
        [Column(StringLength =255)]
        public string AppSecret { get; set; }
        /// <summary>
        /// 支付商户号 字符长度不能超过32个字符
        /// </summary>
        [Description(@"支付商户号")]
        [Column(StringLength =32)]
        public string MerchantId { get; set; }
        /// <summary>
        /// 支付商户秘钥 字符长度不能超过255个字符
        /// </summary>
        [Description(@"支付商户秘钥")]
        [Column(StringLength =255)]
        public string MerchantKey { get; set; }
        /// <summary>
        /// 证书路径 字符长度不能超过127个字符
        /// </summary>
        [Description(@"证书路径")]
        [Column(StringLength =127)]
        public string SslCertPath { get; set; }
        /// <summary>
        /// 证书密码 字符长度不能超过255个字符
        /// </summary>
        [Description(@"证书密码")]
        [Column(StringLength =255)]
        public string SslPassword { get; set; }
        /// <summary>
        /// 服务器IP地址（选填） 字符长度不能超过32个字符
        /// </summary>
        [Description(@"服务器IP地址（选填）")]
        [Column(StringLength =32)]
        public string ServerIP { get; set; }
        /// <summary>
        /// 代理IP+端口号 字符长度不能超过32个字符
        /// </summary>
        [Description(@"代理IP+端口号")]
        [Column(StringLength =32)]
        public string ProxyUrl { get; set; }
        /// <summary>
        /// 客户回调地址（可选） 字符长度不能超过127个字符
        /// </summary>
        [Description(@"客户回调地址（可选）")]
        [Column(StringLength =127)]
        public string CustomNotifyUrl { get; set; }
        /// <summary>
        /// 异步通知回调地址（可选） 字符长度不能超过127个字符
        /// </summary>
        [Description(@"异步通知回调地址（可选）")]
        [Column(StringLength =127)]
        public string NotifyUrl { get; set; }
        /// <summary>
        /// 取消支付跳转地址 字符长度不能超过127个字符
        /// </summary>
        [Description(@"取消支付跳转地址")]
        [Column(StringLength =127)]
        public string QuitUrl { get; set; }
        /// <summary>
        /// 备注信息 字符长度不能超过255个字符
        /// </summary>
        [Description(@"备注信息")]
        [Column(StringLength =255)]
        public string Mark { get; set; }
    }
}

