﻿  //------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。 
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//     如存在本生成代码外的新需求，请在相同命名空间下创建同名分部类实现
// </auto-generated>
//
// <copyright>
//        Copyright(c)2021 XinMai.All rights reserved.
//        CLR版本：4.5
//        开发组织：上海辛麦信息科技@信息中心
//        公司名称：上海辛麦信息科技 
//        所属项目：T4模板引擎
//        当前工程：XinMai.Entity.Paycenter -- 数据实体
//        生成时间：2021-04-29 16:50:53:511
//        作者：Dragon（周龙） QQ：1033085514，手机：15300700700，微信：15300700700 
//        地址：上海市-奉贤区
// </copyright>
//------------------------------------------------------------------------------   

using System;
using System.ComponentModel; 
using FreeSql.DataAnnotations;
namespace XinMai.Entity.Paycenter 
{
    /*
	数据库表映射实体 --支付平台第三方提交订单信息中心表
	*/ 
	[Description(@"分布式代码 -- 支付平台第三方提交订单信息中心表")]
	public partial class PayOrder : Entity<long>
    {
        /// <summary>
        /// 多租户惟一编号 
        /// </summary>
        [Description(@"多租户惟一编号")]
        public int? TenantId { get; set; }
        /// <summary>
        /// 买家惟一编号 
        /// </summary>
        [Description(@"买家惟一编号")]
        public int? BuyerId { get; set; }
        /// <summary>
        /// 商户惟一编码 字符长度不能超过64个字符
        /// </summary>
        [Description(@"商户惟一编码")]
        [Column(StringLength = 64)]
        public string MchCode { get; set; }
        /// <summary>
        /// 买家手机号码 字符长度不能超过16个字符
        /// </summary>
        [Description(@"买家手机号码")]
        [Column(StringLength =16)]
        public string Mobile { get; set; }
        /// <summary>
        /// 支付标题 字符长度不能超过127个字符
        /// </summary>
        [Description(@"支付标题")]
        [Column(StringLength =127)]
        public string Subject { get; set; }
        /// <summary>
        /// 订单号 字符长度不能超过64个字符
        /// </summary>
        [Description(@"订单号")]
        [Column(StringLength = 64)]
        public string OrderCode { get; set; }
        /// <summary>
        /// 支付中心单号 字符长度不能超过64个字符
        /// </summary>
        [Description(@"支付中心单号")]
        [Column(StringLength =64)]
        public string OutTradeNo { get; set; }
        /// <summary>
        /// 订单金额，以分为单位 
        /// </summary>
        [Description(@"订单金额，以分为单位")]
        public int? TotalAmount { get; set; }
        /// <summary>
        /// 客户端项目类型 字符长度不能超过32个字符
        /// </summary>
        [Description(@"客户端项目类型")]
        [Column(StringLength =32)]
        public string ProjectType { get; set; }
        /// <summary>
        /// 业务终端(IOS,Android,PC,H5,小程序) 
        /// </summary>
        [Description(@"业务终端(IOS,Android,PC,H5,小程序)")]
        public int? TerminalType { get; set; }
        /// <summary>
        /// 支付通道类型(余额,支付宝,微信,京东,银联) 
        /// </summary>
        [Description(@"支付通道类型(余额,支付宝,微信,京东,银联)")]
        public int? PayChannelType { get; set; }
        /// <summary>
        /// 支付状态(未支付,部分支付,支付完成) 
        /// </summary>
        [Description(@"支付状态(未支付,部分支付,支付完成)")]
        public int? PayStatusType { get; set; }
        /// <summary>
        /// 订单状态(进行中,已取消,待取消,待评价,部分完成,完成,关闭) 
        /// </summary>
        [Description(@"订单状态(进行中,已取消,待取消,待评价,部分完成,完成,关闭)")]
        public int? OrderStatusType { get; set; }
        /// <summary>
        /// 第三方返回流水单号 字符长度不能超过64个字符
        /// </summary>
        [Description(@"第三方返回流水单号")]
        [Column(StringLength =64)]
        public string ResultCode { get; set; }
        /// <summary>
        /// 备注信息 字符长度不能超过255个字符
        /// </summary>
        [Description(@"备注信息")]
        [Column(StringLength =255)]
        public string Mark { get; set; }
    }
}

