﻿
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace XinMai.Entity.Hairong
{
    public partial class UserInfo
    {
        /// <summary>
        /// 发布的职位数
        /// </summary>
        [Column(IsIgnore = true)]
        public long JobsCount { get; set; }
    }

}
