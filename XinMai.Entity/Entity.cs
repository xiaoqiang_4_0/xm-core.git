﻿using FreeSql.DataAnnotations;
using System;
using XinMai.FreeSql;

namespace XinMai.Entity
{
    /// <summary>
    /// 实体基类结构体
    /// </summary>
    /// <typeparam name="D"></typeparam>
    public abstract class Entity<D> : IEntity<D> 
    {
        protected Entity()
        {
        }
        [Column(IsPrimary = true, IsIdentity = true, Position = 1)]
        public virtual D Id { get; set; }
        [Column(Position = -2)]
        public DateTime? CreateTime { get; set; } = DateTime.Now;
        [Column(Position = -1)]
        public bool? IsFrozen { get; set; } = false;
         
    }
}
