﻿  //------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。 
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//     如存在本生成代码外的新需求，请在相同命名空间下创建同名分部类实现
// </auto-generated>
//
// <copyright>
//        Copyright(c)2021 XinMai.All rights reserved.
//        CLR版本：4.5
//        开发组织：上海辛麦信息科技@信息中心
//        公司名称：上海辛麦信息科技 
//        所属项目：T4模板引擎
//        当前工程：XinMai.Entity.Saasbase -- 数据实体
//        生成时间：2021-04-29 16:33:11:607
//        作者：Dragon（周龙） QQ：1033085514，手机：15300700700，微信：15300700700 
//        地址：上海市-奉贤区
// </copyright>
//------------------------------------------------------------------------------   

using System;
using System.ComponentModel; 
using FreeSql.DataAnnotations;
namespace XinMai.Entity.Saasbase 
{
    /*
	数据库表映射实体 --功能模块具体事件信息表
	*/ 
	[Description(@"分布式代码 -- 功能模块具体事件信息表")]
	public partial class MenuRule : Entity<long>
    {
        /// <summary>
        /// 编码名称 字符长度不能超过64个字符
        /// </summary>
        [Description(@"编码名称")]
        [Column(StringLength =64)]
        public string Code { get; set; }
        /// <summary>
        /// 模块功能名称 字符长度不能超过32个字符
        /// </summary>
        [Description(@"模块功能名称")]
        [Column(StringLength =32)]
        public string Name { get; set; }
        /// <summary>
        /// 事件名称 字符长度不能超过64个字符
        /// </summary>
        [Description(@"事件名称")]
        [Column(StringLength =64)]
        public string ActionName { get; set; }
        /// <summary>
        /// 控制器名称 字符长度不能超过64个字符
        /// </summary>
        [Description(@"控制器名称")]
        [Column(StringLength =64)]
        public string ControllerName { get; set; }
        /// <summary>
        /// 执行方式 
        /// </summary>
        [Description(@"执行方式")]
        public int? Method { get; set; }
        /// <summary>
        /// 模块功能介绍 字符长度不能超过255个字符
        /// </summary>
        [Description(@"模块功能介绍")]
        [Column(StringLength =255)]
        public string Desription { get; set; }
        /// <summary>
        /// 模块功能是否锁定 
        /// </summary>
        [Description(@"模块功能是否锁定")]
        public bool? IsLock { get; set; }
        /// <summary>
        /// 备注信息 字符长度不能超过255个字符
        /// </summary>
        [Description(@"备注信息")]
        [Column(StringLength =255)]
        public string Mark { get; set; }
    }
}

