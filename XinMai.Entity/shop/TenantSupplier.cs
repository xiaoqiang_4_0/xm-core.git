﻿  //------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。 
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//     如存在本生成代码外的新需求，请在相同命名空间下创建同名分部类实现
// </auto-generated>
//
// <copyright>
//        Copyright(c)2021 XinMai.All rights reserved.
//        CLR版本：4.5
//        开发组织：上海辛麦信息科技@信息中心
//        公司名称：上海辛麦信息科技 
//        所属项目：T4模板引擎
//        当前工程：XinMai.Entity.Shop -- 数据实体
//        生成时间：2021-04-29 22:19:43:161
//        作者：Dragon（周龙） QQ：1033085514，手机：15300700700，微信：15300700700 
//        地址：上海市-奉贤区
// </copyright>
//------------------------------------------------------------------------------   

using System;
using System.ComponentModel; 
using FreeSql.DataAnnotations;
namespace XinMai.Entity.Shop 
{
    /*
	数据库表映射实体 --供应商信息表
	*/ 
	[Description(@"分布式代码 -- 供应商信息表")]
	public partial class TenantSupplier : Entity<long>
    {
        /// <summary>
        /// 租户主键 
        /// </summary>
        [Description(@"租户主键")]
        public int? TenantId { get; set; }
        /// <summary>
        /// 模块惟一编号 
        /// </summary>
        [Description(@"模块惟一编号")]
        public int? ModuleId { get; set; }
        /// <summary>
        /// 供应商惟一编号 
        /// </summary>
        [Description(@"供应商惟一编号")]
        public int? SupplierId { get; set; }
        /// <summary>
        /// 提现扣除百分比 
        /// </summary>
        [Description(@"提现扣除百分比")]
        public decimal? WithdrawRatio { get; set; }
        /// <summary>
        /// 审核状态(1-申请,2-审核通过,3-审核失败) 
        /// </summary>
        [Description(@"审核状态(1-申请,2-审核通过,3-审核失败)")]
        public int? AuditType { get; set; }
        /// <summary>
        /// 是否正常启用中 
        /// </summary>
        [Description(@"是否正常启用中")]
        public bool? IsLock { get; set; }
        /// <summary>
        /// 保证金 
        /// </summary>
        [Description(@"保证金")]
        public decimal? EarnestMoney { get; set; }
        /// <summary>
        /// 收益毛利润金额(不含保证金)-提现扣除手续费=纯提现金额 
        /// </summary>
        [Description(@"收益毛利润金额(不含保证金)-提现扣除手续费=纯提现金额")]
        public decimal? Money { get; set; }
        /// <summary>
        /// 待收益金额(订单未完成结单) 
        /// </summary>
        [Description(@"待收益金额(订单未完成结单)")]
        public decimal? WantMoney { get; set; }
        /// <summary>
        /// 备注信息 字符长度不能超过255个字符
        /// </summary>
        [Description(@"备注信息")]
        [Column(StringLength =255)]
        public string Mark { get; set; }
    }
}

